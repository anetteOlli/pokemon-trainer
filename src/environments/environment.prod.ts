export const environment = {
  production: true,
  pokemonApiURL: 'https://pokeapi.co/api/v2/',
  pokemonBaseImageUrl:
    'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon',
  pokemonSpriteBaseUrl:
    'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon',
};
