import { Component, OnInit, Input } from '@angular/core';
import { PokemonApp } from 'src/app/models/pokemon-app.model';

/**
 * Creates a simple card with name and sprite.
 * If the pokemon.sprite is present, it will use it for rendering the image.
 * This component can also be used when the pokemon.sprite property is missing, but the
 * pokemon.url has been overwritten with the sprite url.
 * @param [Pokemon] pokemon - pokemon object.
 */
@Component({
  selector: 'app-pokemon-item',
  templateUrl: './pokemon-item.component.html',
  styleUrls: ['./pokemon-item.component.scss'],
})
export class PokemonItemComponent implements OnInit {
  constructor() {}

  @Input() pokemon: PokemonApp;

  async ngOnInit() {}
}
