import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormControl,
  Validators,
  AbstractControl,
} from '@angular/forms';
import { AuthService } from '../../services/auth.service';

/**
 * Renders the register component.
 * The user is asked to submit the gender, this is however not currently stored.
 * The submitted name is stored in localstorage and authService will update it's observable user.
 */
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  user = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.minLength(3)]),
    gender: new FormControl('Boy', [Validators.required]),
  });

  get name(): AbstractControl {
    return this.user.get('name');
  }

  get gender(): AbstractControl {
    return this.user.get('gender');
  }

  registerUser() {
    if (!this.name.invalid && !this.gender.invalid) {
      this.authService.storeUserInLocalStorage(this.name.value);
    }
  }
  constructor(private authService: AuthService) {}

  ngOnInit(): void {}
}
