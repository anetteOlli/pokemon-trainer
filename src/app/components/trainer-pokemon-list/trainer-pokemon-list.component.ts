import { Component, OnInit, OnDestroy } from '@angular/core';
import { PokemonApp } from 'src/app/models/pokemon-app.model';
import { PokemonService } from 'src/app/services/pokemon.service';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';

/**
 * Renders the trainer page.
 * Recieves the pokemons stored in localstorage through pokemonService.
 * Recieves the username of the trainer by subscribing to the user name from
 * authService
 * Clicking on the pokemons will redirect the user to the catalog/:pokemon name for the
 * latest information about the specified pokemon.
 */
@Component({
  selector: 'app-trainer-pokemon-list',
  templateUrl: './trainer-pokemon-list.component.html',
  styleUrls: ['./trainer-pokemon-list.component.scss'],
})
export class TrainerPokemonListComponent implements OnInit, OnDestroy {
  public pokemonList: PokemonApp[] = [];
  public trainerName: string;
  public subscription: Subscription;

  constructor(
    private pokemonService: PokemonService,
    private authService: AuthService
  ) {
    this.subscription = this.authService.user.subscribe((value) => {
      this.trainerName = value.name;
    });
  }

  ngOnInit(): void {
    this.pokemonList = this.pokemonService.getPokemonsFromLocalStorage();
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
