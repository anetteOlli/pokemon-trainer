import { Component, OnInit } from '@angular/core';
import { PokemonService } from 'src/app/services/pokemon.service';
import { environment } from 'src/environments/environment';

/**
 * Renders a catalog list of pokemons based on the pokapi.co
 * Initially renders 20 pokemons, but pressing button "show me more",
 * will make the component render more pokemons.
 */
@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.scss'],
})
export class PokemonListComponent implements OnInit {
  constructor(private pokemonService: PokemonService) {}

  public pokemonList: any = [];
  public errorMessage: string = '';
  public page: number = 0;
  public isLoading: boolean = false;

  async ngOnInit(): Promise<any> {
    this.refreshPokemonList();
  }

  nextPage(): void {
    this.page++;
    this.refreshPokemonList();
  }
  async refreshPokemonList(): Promise<any> {
    try {
      this.isLoading = true;
      this.pokemonList.push(
        ...(await this.pokemonService.getPokemons(this.page))
      );
    } catch (error) {
      this.errorMessage = error.message ? error.message : error;
    } finally {
      this.isLoading = false;
    }
  }
}
