import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { PokemonService } from '../../services/pokemon.service';
import { PokemonApp } from 'src/app/models/pokemon-app.model';

/**
 * Renders detailed information about a pokemon.
 * Collects the latest information about the pokemon from the pokeapi.co
 */
@Component({
  selector: 'app-pokemon-detailed',
  templateUrl: './pokemon-detailed.component.html',
  styleUrls: ['./pokemon-detailed.component.scss'],
})
export class PokemonDetailedComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private pokemonService: PokemonService,
    private router: Router
  ) {}

  public name: string;
  public pokemon: PokemonApp;
  public errorMessage: string = '';
  public isLoading: boolean = false;
  public flavourText: string = '';

  public showTypes: boolean = false;
  public showBaseStats: boolean = false;
  public showAbilities: boolean = false;
  public showMoves: boolean = false;

  async ngOnInit() {
    this.route.params.subscribe(
      (params: Params) => (this.name = params['name'])
    );
    try {
      this.errorMessage = '';
      this.isLoading = true;
      this.pokemon = await this.pokemonService.getPokemonByName(this.name);
      this.flavourText = await this.pokemonService.getPokemonFlavourTextByName(
        this.name
      );
    } catch (error) {
      this.errorMessage = error;
    } finally {
      this.isLoading = false;
    }
  }
  collectPokemon(): void {
    this.errorMessage = '';
    try {
      this.pokemonService.storePokemonInLocalStorage(this.pokemon);
      this.router.navigate(['trainer']);
    } catch (error) {
      this.errorMessage = error.message;
    }
  }
}
