import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map, tap, catchError } from 'rxjs/operators';
import { PokemonApp } from '../models/pokemon-app.model';

@Injectable({
  providedIn: 'root',
})
export class PokemonService {
  constructor(private http: HttpClient) {}

  public async getPokemons(page: number): Promise<PokemonApp[]> {
    try {
      const regEx = /https:\/\/pokeapi.co\/api\/v2\/pokemon\/|\//g;
      let pokemonListTemp = await this.http
        .get<PokemonApp[]>(
          `${environment.pokemonApiURL}pokemon/?limit=20&offset=${20 * page}`
        )
        .pipe(map((response: any) => response.results))
        .toPromise();

      return pokemonListTemp.map((element: PokemonApp) => {
        let id = element.url.replace(regEx, '');
        return {
          id: id,
          name: element.name,
          url: `${environment.pokemonSpriteBaseUrl}/${id}.png`,
        };
      });
    } catch (error) {
      if (error.status > 400) {
        throw new Error('server unavailable, try again later');
      } else {
        throw new Error('something went horrible wrong');
      }
    }
  }

  public getPokemonByName(name: string): Promise<PokemonApp> {
    return this.http
      .get<PokemonApp>(`${environment.pokemonApiURL}pokemon/${name}`)
      .toPromise();
  }

  public async getPokemonFlavourTextByName(name: string): Promise<any> {
    let flavourText: string = '';

    await this.http
      .get(`${environment.pokemonApiURL}pokemon-species/${name}`)
      .pipe(
        tap((response: any) => {
          for (let i = 0; i < response.flavor_text_entries.length; i++) {
            if (response.flavor_text_entries[i].language.name === 'en') {
              flavourText = response.flavor_text_entries[i].flavor_text.replace(
                /\n|\f/g,
                ' '
              );
              break;
            }
          }
        }),
        catchError((error) => {
          return error;
        })
      )
      .toPromise();

    return flavourText;
  }

  public storePokemonInLocalStorage(pokemon: PokemonApp): void {
    let pokemons = this.getPokemonsFromLocalStorage();

    if (
      pokemons.findIndex((pokemonInArray) => {
        return pokemonInArray.name === pokemon.name;
      }) < 0
    ) {
      pokemons.push(pokemon);
      localStorage.setItem('pokemons', JSON.stringify(pokemons));
    } else {
      throw new Error('you are already training this pokemon');
    }
  }

  public getPokemonsFromLocalStorage(): PokemonApp[] {
    const pokemons = localStorage.getItem('pokemons');
    return pokemons ? JSON.parse(pokemons) : [];
  }
}
