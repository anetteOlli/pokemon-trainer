import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './guard/auth.guard';
import { RegisterGuard } from './guard/register.guard';

import { PokemonDetailedComponent } from './components/pokemon-detailed/pokemon-detailed.component';
import { PokemonListComponent } from './components/pokemon-list/pokemon-list.component';
import { TrainerPokemonListComponent } from './components/trainer-pokemon-list/trainer-pokemon-list.component';
import { RegisterComponent } from './components/register/register.component';

const routes: Routes = [
  {
    path: 'catalog',
    component: PokemonListComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'catalog/:name',
    component: PokemonDetailedComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'trainer',
    component: TrainerPokemonListComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'register',
    component: RegisterComponent,
    canActivate: [RegisterGuard],
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'register',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
